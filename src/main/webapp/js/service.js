'use strict';

/**
 * App module for application services
 *
 * @type {angular.Module}
 */
var service = angular.module('service', []);